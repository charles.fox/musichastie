# MusicHastie

## TUTORIAL

First, listen to HardDaysNightArrangement.mp3.   

Let's see how this is made.  Take a look at mysongs/hardDaysNightArrangment.mh and mysong/hardDaysNight.mh.   

hardDaysNightArrangment.mh is the top level file which defines SONG, which is what ultimately gets played.   It imports the chord sequences from hardDaysNight.mh and includes them in an arragement. The arragenment is made by applying the chords to the field, then drawing strums and bass arpegios from the field, and vamping drum patterns and fills over its structure.   

hardDaysNight.mh in turn imports other .mh files which define chords and basic progression fragments and progressions.

To compile the Hard Days Night example from these mh files into midi, try:

    ./mh mysongs/hardDaysNightArrangement.mh

This creates hardDaysNightArrangement.mid which can be played with your favourite MIDI player such as

    apt install timidity
    timidity hardDaysNightArrangement.mid

Or for advanced MIDI synthesis e.g. for trance styles use your own setup such as JACK enabled softsyns such as ZynAddSubFX.  An example ZynAddSubFX rendering is provided in hardDaysNightArrangement.mp3 and there are some example config files for this synth in the rendering folder.


## MUSICHASTIE LANGUAGE SUMMARY


### PRIMITIVES
  There are three primitives:
 
 * n  : single note, middle C tonic in C major.  All structures are ultimately defined in terms of these.
 * _  : the field.  Begins in the same default state as a default note as above.
 * SONG : MusicHastie looks for a definition of this, and uses it as the final output to the MIDI file.  

### TRANSFORMS
*  +2   : up two scale degrees
*  -2   : down two scale degree
*  ~1/2 : time stretch, either by integer or rational fraction
*  &5   : key shift up a fifth
*  $+6  : mode shift (eg from std major to aolian, the new mode with root on the old 6th)
*  $h   : scale select: M=major, h=hamronic min, m=melodic min, B=extended blues
*  a3/4 : modify amplitude by a rational fraction between 0 and 1.  Must be in num/den form and in range 0-1.
*  v    : change voice (MIDI channel) by positive or negative integer.
*  R     : retrograde (play backwards)
*  r2    : rotate squence round by 2 terms (ie last 2 terms become first 2 terms)
*  w(3,4): swap terms 3 and 4 over  
*  s/FIND/REPLACE/ : replace FIND terms in STRUCTURE with REPLACE
*  f/ROCK1/ : replace the whole structure by a sequence of copies of ROCK1  (eg to create a drum part of the same length as the structure)
*  f/ROCK1,FILL1,FILL2/ : as above but automatically switch from ROCK1 to (drum) fills of increasing sizes at important transitions
*  *74,1 : MIDI control parameter, eg. 74=filter cutoff for the ZynAddSubFx synth, then increment its value from 0-10.

### FIELDS

Most of the above transforms can read their parameters from the 'field' instead of explicit values, eg:
    +_.VERSE   *74,_.VERSE    $_.VERSE    &_.VERSE
  the field is like a series of inaudible note sustained through the whole piece, whose values can be set in the same way as notes but with special _ intead of n:
    +3._       *74,1._        &3._          
  the field is especially useful for describing chord progressions with transforms like iii=+3._ 

This allows rhymic instruments to read their relative positions from the field, eg. +_.f/4Strums.FIELD

### STRUCTURE
  Transforms are applied by putting them , following by a dot, in front of a structure, eg. SONG = VERSE +2.VERSE &3.VERSE

  Square brackets play in parallel:   SONG = [PIANO BASS]  

  To repeat previous term (with all its transforms), write either VERSE ///  or  VERSE x4


 MH is not intended to be a general Turing powerful language, rather it's a limited term rewrite language of less power than a Turing machine.

  It is intended for describing intuitively perceivable WEAM and POP structures, not higher level/ more complex ones such as patterns OF transform sequences.
  
  Users are encouraged to be creative, eg. use your own scripts to generate .mh files with clever sequences of transforms, if you want to go that route.


